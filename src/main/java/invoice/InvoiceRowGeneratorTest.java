package invoice;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.*;

import java.math.BigDecimal;
import java.text.MessageFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.*;

public class InvoiceRowGeneratorTest {

    @InjectMocks InvoiceService invoiceService;
    @Spy InvoiceRowGenerator generator;
    @Mock InvoiceRowDao dao;
    @Captor ArgumentCaptor<InvoiceRow> valueCapture;

    List<InvoiceRow> invoiceRows;

    @BeforeEach
    public void initMocks() {
        MockitoAnnotations.initMocks(this);
        invoiceRows = new ArrayList<>();
        invoiceService = new InvoiceService(dao, generator);
    }

    @Test
    public void generatorSavesTwoRows() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-10");
        LocalDate periodEnd = asDate("2017-02-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();
        verify(dao, times(2)).save(anyInvoiceRow());
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void generatorSavesPeriod() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-07-10");
        LocalDate periodEnd = asDate("2018-06-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(12)).save(anyInvoiceRow());
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void periodStartsWithDateTen() {
        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-10");
        LocalDate periodEnd = asDate("2017-02-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(2)).save(valueCapture.capture());
        invoiceRows = valueCapture.getAllValues();

        assertThat(invoiceRowAsStringWithDateOnly(invoiceRows),
                is("InvoiceRow{date=2017-01-10}, InvoiceRow{date=2017-02-10}"));
    }

    @Test
    public void periodStartsBeforeDateTen() {

        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-09");
        LocalDate periodEnd = asDate("2017-02-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(2)).save(valueCapture.capture());
        invoiceRows = valueCapture.getAllValues();

        assertThat(invoiceRowAsStringWithDateOnly(invoiceRows),
                is("InvoiceRow{date=2017-01-10}, InvoiceRow{date=2017-02-10}"));
    }

    @Test
    public void periodStartsAfterDateTen() {

        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-11");
        LocalDate periodEnd = asDate("2017-02-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(2)).save(valueCapture.capture());
        invoiceRows = valueCapture.getAllValues();

        assertThat(invoiceRowAsStringWithDateOnly(generator.getInvoiceRows()),
                is("InvoiceRow{date=2017-01-11}, InvoiceRow{date=2017-02-10}"));
    }

    @Test
    public void periodEndsBeforeDateTen() {

        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-01-10");
        LocalDate periodEnd = asDate("2017-02-09");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(1)).save(valueCapture.capture());
        invoiceRows = valueCapture.getAllValues();

        assertThat(invoiceRowAsStringWithDateOnly(generator.getInvoiceRows()),
                is("InvoiceRow{date=2017-01-10}"));
    }

    @Test
    public void periodIsBetweenYears() {

        BigDecimal amount = new BigDecimal(100);
        LocalDate periodStart = asDate("2017-12-01");
        LocalDate periodEnd = asDate("2018-02-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(3)).save(valueCapture.capture());
        invoiceRows = valueCapture.getAllValues();

        assertThat(invoiceRowAsStringWithDateOnly(generator.getInvoiceRows()),
                is("InvoiceRow{date=2017-12-10}, InvoiceRow{date=2018-01-10}, InvoiceRow{date=2018-02-10}"));
    }

    @Test
    public void amountDividesExactlyBetweenInvoiceRows() {

        BigDecimal amount = new BigDecimal(9);
        LocalDate periodStart = asDate("2017-12-01");
        LocalDate periodEnd = asDate("2018-02-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(3)).save(argThat(row -> row.amount.intValue() == 3));
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amountModuloGoesToLastInvoiceRow() {

        BigDecimal amount = new BigDecimal(7);
        LocalDate periodStart = asDate("2017-01-10");
        LocalDate periodEnd = asDate("2017-02-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao).save(argThat(row -> row.amount.intValue() == 3));
        verify(dao).save(argThat(row -> row.amount.intValue() == 4));
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amountModuloIsDividedBetweenLastInvoiceRows() {

        BigDecimal amount = new BigDecimal(11);
        LocalDate periodStart = asDate("2017-01-10");
        LocalDate periodEnd = asDate("2017-03-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao).save(argThat(row -> row.amount.intValue() == 3));
        verify(dao, times(2)).save(argThat(row -> row.amount.intValue() == 4));
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amountIsTooSmallAtInvoiceRow() {

        BigDecimal amount = new BigDecimal(6);
        LocalDate periodStart = asDate("2017-01-10");
        LocalDate periodEnd = asDate("2017-03-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(2)).save(argThat(row -> row.amount.intValue() == 3));
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amountIsBelowFixedValue() {

        BigDecimal amount = new BigDecimal(2);
        LocalDate periodStart = asDate("2017-01-10");
        LocalDate periodEnd = asDate("2017-03-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao).save(argThat(row -> row.amount.intValue() == 2));
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void complexPeriodGeneratesCorrectInvoiceRows() {

        BigDecimal amount = new BigDecimal(90);
        LocalDate periodStart = asDate("2017-09-10");
        LocalDate periodEnd = asDate("2019-05-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(15)).save(argThat(row -> row.amount.intValue() == 4));
        verify(dao, times(6)).save(argThat(row -> row.amount.intValue() == 5));
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void smallAmountDividedForLongPeriod() {

        BigDecimal amount = new BigDecimal(20);

        LocalDate periodStart = asDate("2017-09-10");
        LocalDate periodEnd = asDate("2019-05-10");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(4)).save(argThat(row -> row.amount.intValue() == 3));
        verify(dao, times(2)).save(argThat(row -> row.amount.intValue() == 4));
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void smallAmountDividedForLongPeriodWithComplexDates() {

        BigDecimal amount = new BigDecimal(14);

        LocalDate periodStart = asDate("2017-05-12");
        LocalDate periodEnd = asDate("2017-10-08");

        invoiceService.generateInvoiceRows(amount, periodStart, periodEnd);
        invoiceService.save();

        verify(dao, times(2)).save(argThat(row -> row.amount.intValue() == 3));
        verify(dao, times(2)).save(argThat(row -> row.amount.intValue() == 4));
        verifyNoMoreInteractions(dao);
    }

    @Test
    public void amountIsZero() {

        BigDecimal amount = new BigDecimal(0);

        LocalDate periodStart = asDate("2017-05-12");
        LocalDate periodEnd = asDate("2017-10-08");

        assertThrows(IllegalArgumentException.class, ()->
                invoiceService.generateInvoiceRows(amount, periodStart, periodEnd));
    }

    @Test
    public void amountIsNull() {
        LocalDate periodStart = asDate("2017-05-12");
        LocalDate periodEnd = asDate("2017-10-08");

        assertThrows(IllegalArgumentException.class, ()->
                invoiceService.generateInvoiceRows(null, periodStart, periodEnd));
    }

    @Test
    public void datesAreNull() {
        assertThrows(IllegalArgumentException.class, ()->
                invoiceService.generateInvoiceRows(BigDecimal.TEN, null, null));
    }

    @Test
    public void testingDaoWithMockitoAndMatcherExample() {
        dao.save(new InvoiceRow(new BigDecimal(1), LocalDate.now()));

        verify(dao).save(argThat(row -> row.amount.intValue() == 1));

        verify(dao).save(getMatcherForAmount(1));

        verify(dao).save(getMatcherForAmountWithMessage(1));
    }

    private String invoiceRowAsStringWithDateOnly(List<InvoiceRow> invoiceRows) {
        List<String> invoiceDates = new ArrayList<>();
        invoiceRows.forEach(invoiceRow -> invoiceDates.add("InvoiceRow{" +
                "date=" + invoiceRow.date + '}'));
        return String.join(", ", invoiceDates);
    }

    private InvoiceRow getMatcherForAmount(final Integer amount) {
        // Example matcher for testing that argument has certain amount.

        return argThat(row -> row.amount.intValue() == amount);
    }

    private InvoiceRow getMatcherForAmountWithMessage(final Integer amount) {
        // Example matcher for testing that argument has certain amount.
        // With message

        return argThat(new ArgumentMatcher<InvoiceRow>() {

            @Override
            public boolean matches(InvoiceRow row) {
                return amount.equals(row.amount.intValue());
            }

            @Override
            public String toString() {
                return MessageFormat.format(
                        "InvoiceRow with amount {0}", amount);
            }
        });
    }

    private LocalDate asDate(String string) {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        return LocalDate.parse(string, formatter);
    }

    private InvoiceRow anyInvoiceRow() {
        return (InvoiceRow) any();
    }

    private BigDecimal anyAmount() {return new BigDecimal(anyInt());}
}