package invoice;

import java.math.BigDecimal;
import java.time.LocalDate;

public class InvoiceService{

    private InvoiceRowDao dao;
    private InvoiceRowGenerator generator;
    private LocalDate periodStart, periodEnd;
    private BigDecimal amount;

    public InvoiceService(){}
    public InvoiceService(InvoiceRowDao dao, InvoiceRowGenerator generator){
        this.dao = dao;
        this.generator = generator;
    }

    public void save() {
        generator.getInvoiceRows().forEach(iR ->{dao.save(iR);
        System.out.println(iR.toString());});
    }

    public InvoiceRowGenerator generateInvoiceRows(BigDecimal amount, LocalDate periodStart, LocalDate periodEnd){
        if (amount == null || amount.equals(BigDecimal.ZERO) )
            throw new IllegalArgumentException("Amount can not be zero or null!");
        if (periodStart == null || periodEnd == null)
            throw  new IllegalArgumentException("Dates can not be null!");

        this.amount = amount;
        this.periodStart = periodStart;
        this.periodEnd = periodEnd;

        return generator.generateInvoiceRows(amount,periodStart,periodEnd);
    }
}
