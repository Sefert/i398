package invoice;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Period;
import java.util.ArrayList;
import java.util.List;

public class InvoiceRowGenerator{

    private List<InvoiceRow> invoiceRows;
    private BigDecimal amountModulo = BigDecimal.ZERO, amountBase;
    private BigDecimal periodInMonths = BigDecimal.ONE;
    private Period period;
    public static int DUE_DATE = 10;
    public static int MIN_AMOUNT = 3;

    public InvoiceRowGenerator(){}

    public InvoiceRowGenerator generateInvoiceRows(BigDecimal amount, LocalDate periodStart, LocalDate periodEnd) {
        invoiceRows = new ArrayList<>();

        if (amount.intValue() < 3) {
            periodEnd = periodStart;
            amountBase = amount;
        } else {
            period = Period.between(periodStart, periodEnd);
            //+1 period does not include start month
            periodInMonths = new BigDecimal(period.getMonths() + period.getYears()*12 + 1);

            if (periodStart.isAfter(periodStart.withDayOfMonth(DUE_DATE)))
                periodInMonths = periodInMonths.add(BigDecimal.ONE);

            amountModulo = amount.remainder(periodInMonths);
            amountBase = amount.subtract(amountModulo).divide(periodInMonths);

            if (amountBase.longValue() < MIN_AMOUNT) {
                amountBase = new BigDecimal(MIN_AMOUNT);
                amountModulo = amount.remainder(amountBase);
                periodInMonths = (amount.subtract(amountModulo).divide(amountBase));
                periodEnd = periodStart.plusMonths(periodInMonths.intValue()).minusMonths(1);
            }
        }

        for (LocalDate date = periodStart.withDayOfMonth(DUE_DATE); date.isBefore(periodEnd.plusDays(1)); date = date.plusMonths(1)) {
            if (date.isBefore(periodStart))
                invoiceRows.add(new InvoiceRow(amountBase, periodStart));
            else{
                if (date.isAfter(periodStart.withDayOfMonth(DUE_DATE).plusMonths(periodInMonths.subtract(amountModulo).intValue()).minusMonths(1)))
                    invoiceRows.add(new InvoiceRow(amountBase.add(BigDecimal.ONE), date));
                else
                    invoiceRows.add(new InvoiceRow(amountBase, date));
            }
        }

        return this;
    }

    public List<InvoiceRow> getInvoiceRows() {
        return invoiceRows;
    }

}


