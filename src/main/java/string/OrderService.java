package string;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

@SuppressWarnings("unused")
public class OrderService {

    private DataSource dataSource;

    public OrderService(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public List<Order> getFilledOrders() {
        List<Order> filledOrders = dataSource.getOrders().stream().filter(dS -> dS.isFilled()).collect(Collectors.toList());
        return filledOrders;
    }

    public List<Order> getOrdersOver(double amount) {
        List<Order> ordersOver = dataSource.getOrders().stream().filter(dS -> dS.getTotal() > 100).collect(Collectors.toList());
        return ordersOver;
    }

    public List<Order> getOrdersSortedByDate() {
        //sorted((d1,d2)->d1.getOrderDate().compareTo(d2.getOrderDate()))
        List<Order> ordersByDate = dataSource.getOrders().stream().sorted(Comparator.comparing(Order::getOrderDate)).collect(Collectors.toList());
        return ordersByDate;
    }
}
