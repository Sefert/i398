package custommock;

import common.BankService;
import common.Money;

import java.util.Map;

public class TestableBankService implements BankService {

    public Money withdrawAmount, depositAmount;
    public String fromAccount, toAccount;
    public boolean getWithdrawCalled = false;
    public Map<String,Money> accounts;

    @Override
    public void withdraw(Money money, String fromAccount) {
        System.out.println("withdraw called with: " + money + " and " + fromAccount);

        // here the arguments (money and fromAccount) must be remembered
        withdrawAmount = money;
        this.fromAccount = fromAccount;

        //throw new IllegalStateException("not implemented yet");
    }

    @Override
    public void deposit(Money money, String toAccount) {
        System.out.println("deposit called with: " + money + " and " + toAccount);

        // here the arguments (money and toAccount) must be remembered
        depositAmount = money;
        this.toAccount = toAccount;
        if (this.fromAccount.equals(fromAccount)){
            if (withdrawAmount.getAmount() >= money.getAmount())
                getWithdrawCalled = true;
        }
    }

    public boolean wasWithdrawCalledWith(Money money, String account) {

        // here you have to compare current arguments with the ones remembered
        if (withdrawAmount.equals(money) && fromAccount.equals(account))
            return true;
        else
            return false;
    }

    public boolean wasDepositCalledWith(Money money, String account) {

        // here you have to compare current arguments with the ones remembered
        if (depositAmount.equals(money) && toAccount.equals(account))
            return true;
        else
            return false;
    }

    @Override
    public Money convert(Money money, String targetCurrency) {
        if (money.getCurrency().equals(targetCurrency)) return money;

        double rate = 1.0/10;

        return new Money((int) (money.getAmount() / rate), targetCurrency);
    }

    @Override
    public String getAccountCurrency(String account) {
        switch (account) {
            case "E_123": return "EUR";
            case "S_456": return "SEK";
            default: throw new IllegalStateException();
        }
    }

    @Override
    public boolean hasSufficientFundsFor(Money requiredAmount, String account) {
        return true;
    }

    public void setSufficientFundsAvailable(boolean areFundsAvailable) {
        // at the moment hasSufficientFundsFor() returns always true.
        // this method is for configuring how hasSufficientFundsFor() responds.
    }

    public boolean wasWithdrawCalled() {
        // This method should say whether withdraw() method was called
        // (without considering arguments)
        return getWithdrawCalled;
    }

}