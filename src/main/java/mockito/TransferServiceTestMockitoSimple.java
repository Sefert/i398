package mockito;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.CoreMatchers.*;
import static org.mockito.Mockito.*;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import common.Money;

@SuppressWarnings("unused")
public class TransferServiceTestMockitoSimple {

    BankService bankService = mock(BankService.class);
    TransferService transferService = new TransferService(bankService);

    @Test
    public void transferSuccessScenario() {
        when(bankService.getBalance("from")).thenReturn(2000);
        transferService.transferMoney(1000,"from","to");
        verify(bankService).transfer(1000,"from","to");
    }

    @Test
    public void transferringNegativeAmountFails() {
        when(bankService.getBalance("from")).thenReturn(2000);
        transferService.transferMoney(-1000,"from","to");
        verify(bankService, never()).transfer(1000,"from","to");
    }

    @Test
    public void transferFailsWhenNotEnoughFunds() {
        when(bankService.getBalance("from")).thenReturn(500);
        transferService.transferMoney(1000,"from","to");
        verify(bankService, never()).transfer(1000,"from","to");
    }

    private Money anyMoney(){ return any();}

}

interface BankService {

    int getBalance(String formAccount);

    void transfer(int amount, String formAccount, String toAccount);

}

class TransferService {

    private BankService bankService;

    public TransferService(BankService bankService) {
        this.bankService = bankService;
    }

    public void transferMoney(int amount, String formAccount, String toAccount) {

        if (amount <= 0 || formAccount.equals(toAccount))
            return;

        if (bankService.getBalance(formAccount) >= amount) {
            bankService.transfer(amount, formAccount, toAccount);
        }
    }
}
