package stack;

public class Stack{

    private Integer[] stack;
    private int currentPosition;

    public Stack(int size){
        stack = new Integer[size];
        currentPosition = 0;
    }

    public int getSize() {
        return currentPosition;
    }

    public void push(Integer i){
        stack[currentPosition] = i;
        currentPosition++;
    }

    public Integer pop(){
        stackIsEmpty();
        currentPosition--;
        return stack[currentPosition];
    }

    public Integer peek(){
        stackIsEmpty();
        return stack[currentPosition -1];
    }

    private void stackIsEmpty(){
        if (currentPosition == 0)
            throw new IllegalStateException("Stack is Empty!!");
    }
}

