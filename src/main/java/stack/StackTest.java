package stack;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class StackTest {

    @Test
    public void newStackHasNoElements() {
        Stack stack = new Stack(100);
        assertThat(stack.getSize(), is(0));
    }
    @Test
    public void stackHasElements(){
        Stack stack = new Stack(10);
        stack.push(2);
        stack.push(67);
        assertThat(stack.getSize(),is(2));
    }
    @Test
    public void stackPopRemovesElements(){
        Stack stack = new Stack(10);
        stack.push(44);
        stack.push(0);
        stack.pop();
        stack.pop();
        assertThat(stack.getSize(), is(0));
    }
    @Test
    public void stackElmentsAreTheSame(){
        Stack stack = new Stack(10);
        stack.push(20);
        stack.push(7);
        assertThat(stack.pop(), is(7));
        assertThat(stack.pop(), is(20));
    }
    @Test
    public void stackPeekReturnsLastElement(){
        Stack stack = new Stack(20);
        stack.push(0);
        stack.push(578);
        assertThat(stack.peek(), is(578));
    }
    @Test
    public void stackPeekDoesNotRemoveElements(){
        Stack stack = new Stack(100);
        stack.push(0);
        stack.push(578);
        stack.peek();
        assertThat(stack.getSize(), is(2));
    }
    @Test
    public void stackIteratorIsFixed(){
        Stack stack = new Stack(100);
        Integer a;
        stack.push(30);
        stack.push(27);
        a = stack.peek();
        assertThat(stack.peek(), is(a));
    }
    @Test
    public void emptyStackThrowsExceptionOnPop(){
        Stack stack = new Stack(100);
        assertThrows(IllegalStateException.class, () -> stack.pop());
    }
    @Test
    public void emptyStackThrowsExceptionOnPeek(){
        Stack stack = new Stack(100);
        assertThrows(IllegalStateException.class, () -> stack.peek());
    }
}

