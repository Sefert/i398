package rpn;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class RpnCalculatorTest {

    @Test
    public void newCalculatorHasZeroInItsAccumulator() {
        RpnCalculator c = new RpnCalculator();
        assertThat(c.getAccumulator(), is(0));
    }
    @Test
    public void accumulatorHasSetElement() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(2);
        assertThat(c.getAccumulator(), is(2));
    }

    @Test
    public void calculatorSupportsAddition() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        assertThat(c.getAccumulator(), is(3));
    }

    @Test
    public void calculatorSupportsSubtraction() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(3);
        c.enter();
        c.setAccumulator(1);
        c.minus();
        assertThat(c.getAccumulator(), is(2));
    }

    @Test
    public void calculatorSupportsMultiplication() {
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(3);
        c.enter();
        c.setAccumulator(2);
        c.multiply();
        assertThat(c.getAccumulator(), is(6));
    }

    @Test
    public void calculatorPerformsComplexCalculation1(){
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(1);
        c.enter();
        c.setAccumulator(2);
        c.plus();
        c.setAccumulator(4);
        c.multiply();
        assertThat(c.getAccumulator(), is(12));
    }

    @Test
    public void calculatorPerformsComplexCalculation2(){
        RpnCalculator c = new RpnCalculator();
        c.setAccumulator(4);
        c.enter();
        c.setAccumulator(3);
        c.plus();
        c.setAccumulator(2);
        c.enter();
        c.setAccumulator(1);
        c.plus();
        c.multiply();
        assertThat(c.getAccumulator(), is(21));
    }

    @Test
    public void evaluateSimpleStringExpression() {
        RpnCalculator c = new RpnCalculator();
        assertThat(c.evaluate("1 2 +"), is(3));
    }

    @Test
    public void evaluateStringExpression1() {
        RpnCalculator c = new RpnCalculator();
        assertThat(c.evaluate("1 2 + 4 *"), is(12));
    }

    @Test
    public void evaluateStringExpression2() {
        RpnCalculator c = new RpnCalculator();
        assertThat(c.evaluate("5 1 2 + 4 * + 3 +"), is(20));
    }
}