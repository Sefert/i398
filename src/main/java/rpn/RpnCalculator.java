package rpn;

import java.util.Stack;

class RpnCalculator {

    private Stack<Integer> numbers;
    Integer accumulator;

    public RpnCalculator(){
        numbers = new Stack();
        accumulator = null;
    }

    public int getAccumulator() {
        if (accumulator == null)
            return 0;
        return accumulator;
    }

    public void setAccumulator(int number){
        if (accumulator != null)
            enter();
        accumulator = number;
    }

    public void enter(){
        numbers.push(accumulator);
        accumulator = null;
    }

    public void plus(){
        accumulator += numbers.pop();
    }

    public void minus(){
        accumulator = numbers.pop() - accumulator;
    }

    public void multiply(){
        accumulator *= numbers.pop();
    }

    public int evaluate(String expression){
        RpnCalculator calc = new RpnCalculator();
        for (String element: expression.split(" ")) {
            switch (element){
                case "+":
                    calc.plus();
                    break;
                case "-":
                    calc.minus();
                    break;
                case "*":
                    calc.multiply();
                    break;
                default :
                    calc.setAccumulator(Integer.parseInt(element));
                    break;
            }
        }
        return calc.getAccumulator();
    }
}