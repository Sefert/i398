package stub;

import common.Money;

public class TestableBank extends Bank {
    @Override
    public Money convert(Money money, String toCurrency) {
        if ("EUR".equals(toCurrency))
            if ("EEK".equals(money.getCurrency()))
                return new Money(money.getAmount() / 15, "EUR");
            return money;
    }
}
