package stub;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import org.junit.jupiter.api.Test;

import common.Money;

public class ReportTest {

    @Test
    public void calculatesTotalFromAmounts() {
        TestableReport testableReport = new TestableReport();
        testableReport.setBank(new TestableBank());

        Money total = testableReport.getTotalIncomeBetween(null, null);

        assertThat(total, is(new Money(2, "EUR")));
    }

}
