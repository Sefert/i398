package gol;


import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;

import org.junit.jupiter.api.Test;

@SuppressWarnings("unused")
public class GameOfLifeTest {

    @Test
    public void markAliveCells(){
        Frame frame = new Frame(10,10);
        frame.markAlive(3,7);
        frame.markAlive(6,5);
        assertThat(frame.isAlive(3,7), is(true));
        assertThat(frame.isAlive(6,5), is(true));
        assertThat(frame.isAlive(2,4), is(false));
    }

    @Test
    public void countAliveCellAliveNeighbors(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,4);
        frame.markAlive(4,5);
        frame.markAlive(4,6);
        assertThat(frame.getNeighbourCount(4,4), is(1));
        assertThat(frame.getNeighbourCount(4,5), is(2));
        assertThat(frame.getNeighbourCount(4,6), is(1));
    }

    @Test
    public void countDeadCellAliveNeighbors(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,4);
        frame.markAlive(4,6);
        assertThat(frame.getNeighbourCount(4,5), is(2));
    }

    @Test
    public void nextFrameIsFromSameInstance(){
        Frame frame = new Frame(10,10);
        assertThat(frame.nextFrame(), instanceOf(Frame.class));
    }

    @Test
    public void makeACellDie(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,4);
        frame.markDead(4,4);
        assertThat(frame.isAlive(4,4), is(false));
    }


    @Test
    public void frameCornerGivesNoOutOfBoundsException(){
        Frame frame = new Frame(10,10);
        assertDoesNotThrow(()-> frame.getNeighbourCount(0,0));
        assertDoesNotThrow(()-> frame.getNeighbourCount(0,5));
        assertDoesNotThrow(()-> frame.getNeighbourCount(9,9));
    }

    @Test
    public void overPopulatedCellIsGoingToDie(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,5);
        frame.markAlive(4,6);
        frame.markAlive(4,7);
        frame.markAlive(5,5);
        frame.markAlive(5,6);
        assertThat(frame.cellIsGoingToDie(5,6),is(true));
    }

    @Test
    public void aCellIsGoingToBeBorn(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,5);
        frame.markAlive(4,6);
        frame.markAlive(5,5);
        assertThat(frame.cellIsGoingToBeBorn(5,6),is(true));
    }

    @Test
    public void aCellIsBornInNextFrame(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,5);
        frame.markAlive(4,6);
        frame.markAlive(5,5);
        Frame nextFrame = frame.nextFrame();
        assertThat(frame.nextFrame().isAlive(5,6),is(true));
    }

    @Test
    public void aCellDiesInNextFrame(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,5);
        frame.markAlive(4,6);
        frame.markAlive(4,7);
        frame.markAlive(5,5);
        frame.markAlive(5,6);
        assertThat(frame.nextFrame().isAlive(5,6),is(false));
    }

    @Test
    public void nextFrameEqualsPreviousFrame(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,5);
        frame.markAlive(4,6);
        frame.markAlive(5,5);
        frame.markAlive(5,6);
        assertThat(frame.equals(frame.nextFrame()), is(true));
    }

    @Test
    public void nextFrameDoesNotEqualPreviousFrame(){
        Frame frame = new Frame(10,10);
        frame.markAlive(4,5);
        Frame nextFrame = frame.nextFrame();
        assertThat(frame.isAlive(4,5), is(true));
        assertThat(nextFrame.isAlive(4,5), is(false));
        assertThat(frame.equals(nextFrame), is(false));
    }

    @Test
    public void returnsFrameAsString(){
        Frame frame = new Frame(10,10);
        assertThat(frame.toString(), instanceOf(String.class));
    }

    @Test
    public void returnedFrameIsInCorrectLength(){
        Frame frame = new Frame(10,10);
        //10x10 + 10 for new line
        assertThat(frame.toString().length(), is(110));
    }

    @Test
    public void returnsFrameAsCorrectString(){
        Frame frame = new Frame(2,2);
        frame.markAlive(0,0);
        frame.markAlive(0,1);
        frame.markAlive(1,0);
        frame.markAlive(1,1);
        assertThat(frame.toString(), is("++\n" + "++\n"));
    }

    @Test
    public void returnsFrameAsComplexString(){
        Frame frame = new Frame(6,5);
        frame.markAlive(1,2);
        frame.markAlive(1,3);
        frame.markAlive(2,1);
        frame.markAlive(2,4);
        frame.markAlive(3,2);
        frame.markAlive(3,3);
        assertThat(frame.toString(), is("      \n" + "  ++  \n" + " +  + \n" +
                "  ++  \n" + "      \n"));
    }

    @Test
    public void returnsNextFrameAsComplexString(){
        Frame frame = new Frame(6,5);
        frame.markAlive(1,2);
        frame.markAlive(1,3);
        frame.markAlive(2,1);
        frame.markAlive(2,4);
        frame.markAlive(3,2);
        frame.markAlive(3,3);
        assertThat(frame.nextFrame().toString(), is("      \n" + "  ++  \n" + " +  + \n" +
                "  ++  \n" + "      \n"));
    }
}