package gol;

import java.util.Arrays;

public class
Frame {

    private String[][] gameOfLifeWorld;
    private int rows, columns;
    private Frame oldFrame, nextFrame;

    public Frame(int width, int height) {
        this.columns = width;
        this.rows = height;
        this.gameOfLifeWorld = new String[height][width];
    }

    @Override
    public String toString() {

        //https://stackoverflow.com/questions/14534767/how-to-append-a-newline-to-stringbuilder
        StringBuilder gameOfLife = new StringBuilder();
        for (String[] row : gameOfLifeWorld) {
            for (String cell : row) {
                if (cell == null)
                    gameOfLife.append(" ");
                else
                    gameOfLife.append(cell);
            }
            gameOfLife.append("\n");
        }
        return gameOfLife.toString();
    }

    @Override
    public boolean equals(Object obj) {
        //https://docs.oracle.com/javase/7/docs/api/java/util/Arrays.html#deepEquals%28java.lang.Object%5B%5D,%20java.lang.Object%5B%5D%29
        if (Arrays.deepEquals(((Frame) obj).gameOfLifeWorld, gameOfLifeWorld))
            return true;
        return false;
    }

    public Integer getNeighbourCount(int x, int y) {

        int rowStart, columStart, rowEnd, columnEnd;

        //conditions to exclude out of bounds markers
        rowStart = (x == 0) ? x : x-1;
        columStart = (y == 0) ? y : y-1;
        rowEnd = (x == rows - 1) ? x+1 : x+2;
        columnEnd = (y == columns - 1) ? y+1 : y+2;

        int aliveCells = 0;
        for (int i = rowStart ; i < rowEnd; i++) {
            for (int j = columStart; j < columnEnd; j++) {
                if (isAlive(i, j)) {
                    aliveCells++;
                    if (i == x && j == y)
                        aliveCells--;
                }
            }
        }
        return aliveCells;
    }

    public boolean isAlive(int x, int y) {
        if ("+".equals(gameOfLifeWorld[x][y])){
            return true;
        }
        return false;
    }

    public void markAlive(int x, int y) {
        gameOfLifeWorld[x][y] = "+";
    }
    public void markDead(int x, int y) {
        gameOfLifeWorld[x][y] = null;
    }

    public boolean cellIsGoingToDie(int x, int y) {
        int aliveNeighbours = getNeighbourCount(x,y);
        if (aliveNeighbours < 2 || aliveNeighbours > 3)
            return true;
        return false;
    }

    public boolean cellIsGoingToBeBorn(int x, int y) {
        if (getNeighbourCount(x,y) == 3)
            return true;
        return false;
    }


    public Frame nextFrame() {
        oldFrame = this;
        nextFrame = new Frame(columns,rows);

        for (int i = 0; i < rows ; i++) {
            for (int j = 0; j < columns; j++) {
                if (oldFrame.cellIsGoingToDie(i,j))
                    nextFrame.markDead(i,j);
                else if (oldFrame.cellIsGoingToBeBorn(i,j))
                    nextFrame.markAlive(i,j);
                else
                    nextFrame.gameOfLifeWorld[i][j] = oldFrame.gameOfLifeWorld[i][j];
            }
        }
        return nextFrame;
    }

    // helper for complex solutions
    public static void main(String[] args){
        Frame frame = new Frame(6,5);
        frame.markAlive(1,2);
        frame.markAlive(1,3);
        frame.markAlive(2,1);
        frame.markAlive(2,4);
        frame.markAlive(3,2);
        frame.markAlive(3,3);
        System.out.println(frame.toString());
        System.out.println(frame.nextFrame().toString());
        frame = new Frame(6,6);
        frame.markAlive(1,1);
        frame.markAlive(1,2);
        frame.markAlive(2,1);
        frame.markAlive(3,4);
        frame.markAlive(4,3);
        frame.markAlive(4,4);
        System.out.println(frame.toString());
        System.out.println(frame.nextFrame().toString());
        System.out.println(frame.nextFrame().nextFrame().toString());
        frame = new Frame(4,6);
        frame.markAlive(0,1);
        frame.markAlive(1,2);
        frame.markAlive(1,3);
        frame.markAlive(2,1);
        frame.markAlive(2,2);
        System.out.println(frame.toString());
        System.out.println(frame.nextFrame().toString());
        System.out.println(frame.nextFrame().nextFrame().toString());
        System.out.println(frame.nextFrame().nextFrame().nextFrame().toString());
    }
}

