package refactoring;

import java.util.*;

public class Refactoring {

    // 1a
    public int getNewInvoiceNumber() {
        return ++invoiceNumber;
    }

    // 1b
    public void filledOrdersAsList(List<Order> orderList) {
        for (Order order : orders) {
            if (order.isFilled()) {
                orderList.add(order);
            }
        }
    }

    // 2a
    public void printStatementFor(Long invoiceId) {
        List<InvoiceRow> invoiceRows = dao.getInvoiceRowsFor(invoiceId);

        printInvoiceRows(invoiceRows);
        printValue(calculateInvoiceTotal(invoiceRows));

    }

    private double calculateInvoiceTotal(List<InvoiceRow> invoiceRows) {
        double total = 0;
        for (InvoiceRow invoiceRow : invoiceRows) {
            total += invoiceRow.getAmount();
        }
        return total;
    }

    // 2b
    public String getItemsAsHtml() {
        StringBuilder result = new StringBuilder();
        items.forEach(item -> result.append(createTag("li", item)));
        return createTag("ul" , result.toString());
    }
    private String createTag(String tag, String value){
        return "<" + tag + ">" + value + "</" + tag + ">";
    }

    // 3
    public boolean isSmallOrder() {
        return order.getTotal() > 100;
    }

    // 4
    public void printPrice() {
        final double VAT = 1.2;
        System.out.println("Price not including VAT: " + getBasePrice());
        System.out.println("Price including VAT: " + getBasePrice() * VAT);
    }

    // 5
    public void calculatePayFor(Job job) {
        final boolean isSaturday = job.day == 6;
        final boolean isSunday = job.day == 7;
        final boolean hoursOverWorked = job.hour > 20;
        final boolean hoursUnderWorked = job.hour < 7;
        if ((isSaturday || isSunday)
                && (hoursOverWorked || hoursUnderWorked))  {

        }
    }

    // 6
    public boolean canAccessResource(SessionData sessionData) {
        return isAdmin(sessionData);
    }

    private boolean isAdmin(SessionData sessionData) {
        return (isUsername(sessionData)) && (isStatus(sessionData));
    }

    private boolean isUsername(SessionData sessionData) {
        List<String> adminNames = Arrays.asList("Admin", "Administrator");
        return adminNames.contains(sessionData.getCurrentUserName());
        //https://stackoverflow.com/questions/23407014/return-from-lambda-foreach-in-java
        //return adminNames.stream().anyMatch(name -> name.equals(sessionData.getCurrentUserName()));
    }

    private boolean isStatus(SessionData sessionData) {
        List<String> conditions = Arrays.asList("preferredStatusX", "preferredStatusY");
        return conditions.contains(sessionData.getStatus());
    }

    // 7
    public void drawLines() {
        Space space = new Space();
        space.drawLine(new SpaceCoordinate(12, 3, 5), new SpaceCoordinate(2,4,6));
        space.drawLine(new SpaceCoordinate(2, 4, 6), new SpaceCoordinate(0,1,0));
    }

    // 8
    public int calculateWeeklyPay(int hoursWorked, boolean overtime) {
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int straightPay = straightTime * hourRate;
        double overtimeRate = overtime ? 1.5 * hourRate : 1.0 * hourRate;
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return straightPay + overtimePay;
    }
    public int calculateWeeklyPayWithOvertime(int hoursWorked){
        double overtimeRate = 1.5 * hourRate;
        int straightTime = Math.min(40, hoursWorked);
        int overTime = Math.max(0, hoursWorked - straightTime);
        int overtimePay = (int) Math.round(overTime * overtimeRate);
        return calculateWeeklyPayWithoutOvertime(straightTime) + overtimePay;
    }
    public int calculateWeeklyPayWithoutOvertime(int hoursWorked){
        return hoursWorked * hourRate;
    }

    // //////////////////////////////////////////////////////////////////////////

    // Helper fields and methods.
    // They are here just to make the code compile

    private List<String> items = Arrays.asList("1", "2", "3", "4");
    private int hourRate = 5;
    int invoiceNumber = 0;
    private List<Order> orders = new ArrayList<>();
    private Order order = new Order();
    private Dao dao = new SampleDao();
    private double price = 0;

    void justACaller() {
        getNewInvoiceNumber();
        filledOrdersAsList(null);
    }

    private void printValue(double total) {
    }

    private void printInvoiceRows(List<InvoiceRow> invoiceRows) {
    }

    class Space {
        public void drawLine(SpaceCoordinate start, SpaceCoordinate end) {
        }

    }

    interface Dao {
        List<InvoiceRow> getInvoiceRowsFor(Long invoiceId);
    }

    class SampleDao implements Dao {
        @Override
        public List<InvoiceRow> getInvoiceRowsFor(Long invoiceId) {
            return null;
        }
    }

    class Order {
        public boolean isFilled() {
            return false;
        }

        public double getTotal() {
            return 0;
        }
    }

    class InvoiceRow {
        public double getAmount() {
            return 0;
        }
    }

    class Job {
        public int hour;
        public int day;
    }

    private double getBasePrice() {
        return price;
    }

    private class SessionData {

        public String getCurrentUserName() {
            return null;
        }

        public String getStatus() {
            return null;
        }

    }

}
