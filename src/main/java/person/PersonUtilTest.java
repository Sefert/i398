package person;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static java.util.Arrays.*;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.fail;

public class PersonUtilTest {

    private PersonUtil personUtil = new PersonUtil();

    @Test
    public void findsOldestPerson() {
        Person p1 = aPerson().withAge(32).build();
        Person p2 = aPerson().withAge(55).build();
        Person p3 = aPerson().withAge(21).build();

        assertThat(personUtil.getOldest(asList(p1, p2, p3)), is(p2));
    }

    @Test
    public void findsPersonsInLegalAge() {
        Person p1 = aPerson().withAge(21).build();
        Person p2 = aPerson().withAge(16).build();
        Person p3 = aPerson().withAge(43).build();

        assertThat(personUtil.getPersonsInLegalAge(asList(p1, p2, p3)), is(asList(p1, p3)));
    }

    @Test
    public void findsWomen() {
        Person p1 = aPerson().withGender(Person.GENDER_FEMALE).build();
        Person p2 = aPerson().withGender(Person.GENDER_FEMALE).build();
        Person p3 = aPerson().withGender(Person.GENDER_MALE).build();
        Person p4 = aPerson().withGender(Person.GENDER_FEMALE).build();

        assertThat(personUtil.getWomen(asList(p1, p2, p3, p4)), is(asList(p1, p2, p4)));
    }

    @Test
    public void findsPersonsLivingInSpecifiedTown() {
        Person p1 = aPerson().withTown("Tallinn").build();
        Person p2 = aPerson().withTown("Haapsalu").build();
        Person p3 = aPerson().withTown("Valga").build();
        Person p4 = aPerson().withTown("Tallinn").build();

        assertThat(personUtil.getPersonsWhoLiveIn("Tallinn", asList(p1, p2, p3, p4)), is(asList(p1, p4)));
    }

    private PersonBuilder aPerson() {
        return new PersonBuilder();
    }
}
