package person;

import java.util.Arrays;
import java.util.List;
import java.util.Random;

public class PersonBuilder {

    private String name;
    private Address address;
    private String gender;
    private int age;

    public PersonBuilder withAge(int age) {
        this.age = age;
        return this;
    }

    public PersonBuilder withGender(String gender) {
        this.gender = gender;
        return this;
    }

    public PersonBuilder withTown(String town) {
        List<String> streets = Arrays.asList("Lai","Ristiku","Kolde","Tammsaare");
        this.address = new Address(streets.get(new Random().nextInt(streets.size())), town);
        return this;
    }

    public Person build() {
        if (isNull(gender)){
            List<String> genders = Arrays.asList(Person.GENDER_FEMALE, Person.GENDER_MALE);
            this.gender = genders.get(new Random().nextInt(genders.size()));
        }
        if (isNull(name)){
            if (gender.equals(Person.GENDER_FEMALE)){
                List<String> names = Arrays.asList("Annika","Berit","Tanja","Siiri");
                this.name = names.get(new Random().nextInt(names.size()));
            } else {
                List<String> names = Arrays.asList("Marko","Tom","Sulev","Chris");
                this.name = names.get(new Random().nextInt(names.size()));
            }
        }
        if (age == 0) {
            List<Integer> ages = Arrays.asList(12,15,21,32);
            this.age = ages.get(new Random().nextInt(ages.size()));
        }
        if (isNull(address)){
            List<String> streets = Arrays.asList("Lai","Ristiku","Kolde","Tammsaare");
            List<String> towns = Arrays.asList("Tallinn","Pärnu","Jõgeva","Kuressaare");
            this.address = new Address(streets.get(new Random().nextInt(streets.size())),
                                        towns.get(new Random().nextInt(towns.size())));
        }
        return new Person(name, age, gender, address);
    }
    private boolean isNull(Object obj) {
        return obj == null;
    }
}
